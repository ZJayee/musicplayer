package com.example.goa_project2;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;

import org.w3c.dom.Text;


public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        ImageButton profile = findViewById(R.id.profile);
        profile.setOnClickListener(this::toProfile);

        ImageButton playlist = findViewById(R.id.playlist);
        playlist.setOnClickListener(this::toPlaylist);
        //find songs
        View song1 = findViewById(R.id.song1);
        View song2 = findViewById(R.id.song2);
        View song3 = findViewById(R.id.song3);

        //song image
        ImageView song1image = song1.findViewById(R.id.profile_image);
        ImageView song2image = song2.findViewById(R.id.profile_image);
        ImageView song3image = song3.findViewById(R.id.profile_image);
        //set song image
        song1image.setImageResource(R.drawable.mindgames);
        song2image.setImageResource(R.drawable.kronical);
        song3image.setImageResource(R.drawable.herbaltea);

        //find texts
        TextView song1name = song1.findViewById(R.id.songName);
        TextView song2name = song2.findViewById(R.id.songName);
        TextView song3name = song3.findViewById(R.id.songName);
        //set texts
        song1name.setText("Mind Games");
        song2name.setText("Kronical");
        song3name.setText("Herbal Tea");

        //find artist
        TextView song1aname = song1.findViewById(R.id.artist);
        TextView song2aname = song2.findViewById(R.id.artist);
        TextView song3aname = song3.findViewById(R.id.artist);
        //set artist
        song1aname.setText("Prod. Riddiman");
        song2aname.setText("BassRebels");
        song3aname.setText("Artificial.Music");

        //find albums
        View album1 = findViewById(R.id.album1);
        View album2 = findViewById(R.id.album2);
        View album3 = findViewById(R.id.album3);

        //album image
        ImageView albumimage1 = album1.findViewById(R.id.profile_image);
        ImageView albumimage2 = album2.findViewById(R.id.profile_image);
        ImageView albumimage3 = album3.findViewById(R.id.profile_image);
        //set album image
        albumimage1.setImageResource(R.drawable.dayslikethese);
        albumimage2.setImageResource(R.drawable.oops);
        albumimage3.setImageResource(R.drawable.feelsgood);

        //name
        TextView album1name = album1.findViewById(R.id.songName);
        TextView album2name = album2.findViewById(R.id.songName);
        TextView album3name = album3.findViewById(R.id.songName);
        //set name
        album1name.setText("Days Like These");
        album2name.setText("[oops]");
        album3name.setText("Feel Good");

        //artist name
        TextView album1aname = album1.findViewById(R.id.artist);
        TextView album2aname = album2.findViewById(R.id.artist);
        TextView album3aname = album3.findViewById(R.id.artist);
        //set artist name
        album1aname.setText("LAKEY INSPIRED");
        album2aname.setText("potsu");
        album3aname.setText("Rex Orange County");

        //artist
        View artist1 = findViewById(R.id.artist1);
        View artist2 = findViewById(R.id.artist2);
        View artist3 = findViewById(R.id.artist3);

       //artist image
        ImageView artistimage2 = artist2.findViewById(R.id.artistimage);
        ImageView artistimage3 = artist3.findViewById(R.id.artistimage);
        //set image
        artistimage2.setImageResource(R.drawable.caixukun);
        artistimage3.setImageResource(R.drawable.wangjunkai);

        //artist text
        TextView artistname1 = artist1.findViewById(R.id.artistname);
        TextView artistname2 = artist2.findViewById(R.id.artistname);
        TextView artistname3 = artist3.findViewById(R.id.artistname);
        //set artist name

        artistname1.setText("Lixikan");
        artistname2.setText("Caixukun");
        artistname3.setText("Wangjunkai");




    }


    void toProfile(View view){
        Intent intent = new Intent(this, ProfileActivity.class);
        startActivity(intent);
    }

    void toPlaylist(View view){
        Intent intent = new Intent(this, PlaylistActivity.class);
        startActivity(intent);
    }
}