package com.example.goa_project2;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageButton;

import java.util.ArrayList;

public class PlaylistActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_playlist);
        populateDataModel();
        connectXMLViews();
        setupRecyclerView();

        ImageButton backButton = findViewById(R.id.backButton);
        backButton.setOnClickListener(this::toHome);
        shuffleButton = findViewById(R.id.shuffleButton);

        shuffleButton.setOnClickListener(this::toPlayer);
    }



    Playlist playlist1 = new Playlist();
    RecyclerView songsRecyclerView;
    SongAdapter songAdapter;
    ImageButton shuffleButton;


    void toHome(View view){
        Intent intent = new Intent(this, MainActivity.class);
        startActivity(intent);
    }

    void populateDataModel(){
        //initialize playlist
        playlist1.name = "Playlist Name";
        playlist1.songs = new ArrayList<Song>();

        //create and initialize songs
        Song song1 = new Song("Mind Games","Prod. Riddiman", R.drawable.mindgames, R.raw.mindgames);
        Song song2 = new Song("Im SURF","Take Care", R.drawable.imsurf, R.raw.imsurf);
        Song song3 = new Song("Orca Vibes","Gypsy", R.drawable.orcavibes, R.raw.orcavibes);
        Song song4 = new Song("Herbal Tea","Artificial.Music", R.drawable.herbaltea, R.raw.herbaltea);
        Song song5 = new Song("Kronicle","Lofi Experimentin", R.drawable.kronical, R.raw.kronicle);
        Song song6 = new Song("Serenity","Prod. Riddiman", R.drawable.serenity, R.raw.serenity);
        Song song7 = new Song("chocolate","lukrembo", R.drawable.chocolate, R.raw.chocolate);
        Song song8 = new Song("Portrait","Colde x Anderson Paak R&B", R.drawable.portrait, R.raw.portrait);
        Song song9 = new Song("[oops]","potsu", R.drawable.oops, R.raw.oops);
        Song song10 = new Song("feel good","Rex Orange County", R.drawable.feelsgood, R.raw.mindgames);
        Song song11 = new Song("Chasing Palm Trees","Ehrling", R.drawable.chasingpalmtrees, R.raw.chasingpalmtrees);
        Song song12 = new Song("Road Trip","Joakim Karud", R.drawable.roadtrip, R.raw.roadtrip);
        Song song13 = new Song("In My Dreams","LAKEY INSPIRED", R.drawable.inmydreams, R.raw.inmydreams);
        Song song14 = new Song("Days Like These","LAKEY INSPIRED", R.drawable.dayslikethese, R.raw.dayslikethese);
        Song song15 = new Song("Sorry","Prod. Riddiman", R.drawable.sorry, R.raw.sorry);


        playlist1.songs.add(song1);
        playlist1.songs.add(song2);
        playlist1.songs.add(song3);
        playlist1.songs.add(song4);
        playlist1.songs.add(song5);
        playlist1.songs.add(song6);
        playlist1.songs.add(song7);
        playlist1.songs.add(song8);
        playlist1.songs.add(song9);
        playlist1.songs.add(song10);
        playlist1.songs.add(song11);
        playlist1.songs.add(song12);
        playlist1.songs.add(song13);
        playlist1.songs.add(song14);
        playlist1.songs.add(song15);
//        playlist1.songs.add(song);
    }

    void connectXMLViews(){
        songsRecyclerView = findViewById(R.id.songList);
    }

    void setupRecyclerView(){
        LinearLayoutManager layoutManager = new LinearLayoutManager(this);
        songsRecyclerView.setLayoutManager(layoutManager);

        //connect the adapter to the recyclerView
        songAdapter = new SongAdapter(this, playlist1.songs, this);
        songsRecyclerView.setAdapter(songAdapter);
    }

    void toPlayer(View view){
        int index = (int)(Math.random()*playlist1.songs.size());
        Intent intent = new Intent(this, PlayerScreen.class);
        intent.putExtra("index", index);
        intent.putExtra("playlist", playlist1);
        startActivity(intent);
    }

}