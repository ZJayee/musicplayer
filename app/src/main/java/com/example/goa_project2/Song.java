package com.example.goa_project2;

/*
Class for songs, other variables can be added later on
 */

import java.io.Serializable;

public class Song implements Serializable {
    String name; //name of song
    String artist; //artist
    int artworkImage; //album cover photo source
    int mp3File; //music file source

    public Song(){

    }
    public Song(String name, String artist, int artworkImage, int mp3File){
        this.name = name;
        this.artist = artist;
        this.artworkImage = artworkImage;
        this.mp3File = mp3File;
    }
}
