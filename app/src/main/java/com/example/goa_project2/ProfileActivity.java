package com.example.goa_project2;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;

public class ProfileActivity extends AppCompatActivity {

    ImageButton backButton;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_profile);

        backButton = findViewById(R.id.backButton);
        backButton.setOnClickListener(this::toHome);


        View playlist1 = findViewById(R.id.include4);
        View playlist2 = findViewById(R.id.include54);
        View playlist3 = findViewById(R.id.include5);
        View playlist4 = findViewById(R.id.include6);

        View artist1 = findViewById(R.id.include2);
        View artist2 = findViewById(R.id.include);
        View artist3 = findViewById(R.id.include3);

        //other profilepage stuff

        //FAVS




        TextView profilefavsong1name = artist1.findViewById(R.id.pfsongName);
        TextView profilefavsong2name = artist2.findViewById(R.id.pfsongName);
        TextView profilefavsong3name = artist3.findViewById(R.id.pfsongName);
        profilefavsong1name.setText("Oops");
        profilefavsong2name.setText("Sorry");
        profilefavsong3name.setText("Orca Vibes");
        TextView pfsongartist1 = artist1.findViewById(R.id.pfartistName);
        TextView pfsongartist2 = artist2.findViewById(R.id.pfartistName);
        TextView pfsongartist3 = artist3.findViewById(R.id.pfartistName);
        pfsongartist1.setText("Potsu");
        pfsongartist2.setText("Prod. Riddiman ");
        pfsongartist3.setText("Gypsy");

        TextView pffavtime1 = artist1.findViewById(R.id.pftime);
        TextView pffavtime2 = artist2.findViewById(R.id.pftime);
        TextView pffavtime3 = artist3.findViewById(R.id.pftime);
        pffavtime1.setText("2:22");
        pffavtime2.setText("3:01");
        pffavtime3.setText("1:47");

        ImageView pfartistimage1 = artist1.findViewById(R.id.image);
        ImageView pfartistimage2 = artist2.findViewById(R.id.image);
        ImageView pfartistimage3 = artist3.findViewById(R.id.image);
        pfartistimage1.setImageResource(R.drawable.oops);
        pfartistimage2.setImageResource(R.drawable.sorry);
        pfartistimage3.setImageResource(R.drawable.orcavibes);




        ImageView favplaylists1 = playlist1.findViewById(R.id.playlistpog);
        ImageView favplaylists2 = playlist2.findViewById(R.id.playlistpog);
        ImageView favplaylists3 = playlist3.findViewById(R.id.playlistpog);
        ImageView favplaylists4 = playlist4.findViewById(R.id.playlistpog);
        favplaylists1.setImageResource(R.drawable.chasingpalmtrees);
        favplaylists2.setImageResource(R.drawable.serenity);
        favplaylists3.setImageResource(R.drawable.roadtrip);
        favplaylists4.setImageResource(R.drawable.herbaltea);
    }

    void toHome(View view) {
        Intent intent = new Intent(this, MainActivity.class);
        startActivity(intent);
    }
}