package com.example.goa_project2;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.media.MediaPlayer;
import android.os.Bundle;
import android.os.Handler;
import android.view.View;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.SeekBar;
import android.widget.TextView;

import java.util.Timer;
import java.util.TimerTask;
import java.util.concurrent.TimeUnit;

public class PlayerScreen extends AppCompatActivity {
    ImageButton playButton;
    ImageButton nextButton;
    ImageButton lastButton;
    ImageButton backButton;
    Boolean isPlaying = false;
    int currentSongIndex;
    Playlist playlist;
    SeekBar seekBar;
    //MediaPlayer to play MP3
    MediaPlayer mediaPlayer = null;
    double finalTime;
    Handler handler;
    TextView timeelapsedtext;
    TextView timelefttext;

    //bottom buttons
    ImageButton heartButton;
    ImageButton loopButton;
    ImageButton shuffleButton;

    Boolean isHeart = false;
    Boolean isRepeat = false;
    Boolean isShuffle = false;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_player_screen);

        //properties
        playButton = findViewById(R.id.playButton);
        nextButton = findViewById(R.id.nextButton);
        lastButton = findViewById(R.id.lastButton);
        backButton = findViewById(R.id.backButton);
        seekBar = findViewById(R.id.progress);
        timeelapsedtext = findViewById(R.id.timeelapsed);
        timelefttext = findViewById(R.id.timeleft);
        heartButton = findViewById(R.id.heartButton);
        loopButton = findViewById(R.id.loopButton);
        shuffleButton = findViewById(R.id.shuffleButton);

        Intent intent = getIntent();
        Bundle extras = intent.getExtras();
        currentSongIndex = extras.getInt("index");
        playlist = (Playlist)getIntent().getSerializableExtra("playlist");

        //set display to clicked content
        Song currentSong = playlist.songs.get(currentSongIndex);
        ImageView albumImage = findViewById(R.id.albumImage);
        albumImage.setImageResource(currentSong.artworkImage);
        TextView songName = findViewById(R.id.songName);
        songName.setText(currentSong.name);
        TextView artist = findViewById(R.id.artistname);
        artist.setText(currentSong.artist);

        handler = new Handler();


        backButton.setOnClickListener(this::toPlaylist);

        playButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
            playSong();
            }
        });

        nextButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
            nextSong();
            }
        });

        lastButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
            lastSong();
            }
        });

        seekBar.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            int seeked_progress;
            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                seeked_progress = progress;

            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {

            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {
                mediaPlayer.seekTo(seeked_progress);
            }
        });

        heartButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(isHeart){
                    isHeart = false;
                    heartButton.setImageResource(R.drawable.ic_heart);
                }else{
                    isHeart = true;
                    heartButton.setImageResource(R.drawable.ic_heart2);
                }
            }
        });

        loopButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(isRepeat){
                    isRepeat = false;
                    loopButton.setImageResource((R.drawable.ic_loop));
                }else{
                    isRepeat = true;
                    loopButton.setImageResource(R.drawable.ic_repeat2);
                }
            }
        });

        shuffleButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(isShuffle){
                    isShuffle=false;
                    shuffleButton.setImageResource(R.drawable.ic_shuffle);
                }else{
                    isShuffle = true;
                    shuffleButton.setImageResource(R.drawable.ic_shuffle2);
                }
            }
        });

    }


    void playSong(){

        if(isPlaying == false){
            isPlaying = true;
            playButton.setImageResource(R.drawable.ic_pause);


            if(mediaPlayer==null){
                //get song object corresponding to current song
                Song currentSong = playlist.songs.get(currentSongIndex);

                //media player for the mp3 resource of the current song
                mediaPlayer = MediaPlayer.create(PlayerScreen.this, currentSong.mp3File);
            }
            finalTime = mediaPlayer.getDuration();
            seekBar.setMax(0);
            seekBar.setMax((int)finalTime);
            updateSeekBar();
            mediaPlayer.start();
        }else{


            if(mediaPlayer!=null){
                isPlaying = false;
                playButton.setImageResource(R.drawable.ic_play);
                mediaPlayer.pause();
            }
        }
    }

    void nextSong(){
        if(currentSongIndex<playlist.songs.size()-1) {
            switchSong(currentSongIndex,currentSongIndex+1);
        }else{
            switchSong(currentSongIndex,0);
        }
    }

    void lastSong(){
        if(currentSongIndex>0) {
            switchSong(currentSongIndex,currentSongIndex-1);
        }else{
            switchSong(currentSongIndex,playlist.songs.size()-1);
        }
    }

    void switchSong(int fromIndex, int toIndex){
        currentSongIndex = toIndex;
        seekBar.setProgress(0);


        if(mediaPlayer!=null && mediaPlayer.isPlaying()) {
            playSong();
            mediaPlayer = null;
            playSong();
            setTime();

        }else{
            mediaPlayer = null;
            handler.removeCallbacksAndMessages(null);

            timelefttext.setText("0:00");
            timeelapsedtext.setText("0:00");
        }

        displayCurrentSong();
    }

    void displayCurrentSong(){
        Song currentSong = playlist.songs.get(currentSongIndex);
        ImageView albumImage = findViewById(R.id.albumImage);
        albumImage.setImageResource(currentSong.artworkImage);
        TextView songName = findViewById(R.id.songName);
        songName.setText(currentSong.name);
        TextView artist = findViewById(R.id.artistname);
        artist.setText(currentSong.artist);
    }

    void toPlaylist(View view) {
        Intent intent = new Intent(this, PlaylistActivity.class);
        startActivity(intent);
    }

    void updateSeekBar(){
        seekBar.setProgress(mediaPlayer.getCurrentPosition());

        handler.postDelayed(runnable ,50);
        updateTime();
    }

    Runnable runnable = new Runnable() {
        @Override
        public void run() {
            updateSeekBar();

        }
    };

    void setTime(){
        double totalTime = mediaPlayer.getDuration()/1000.0;
        String minleft = Integer.toString((int)totalTime/60);
        String secondleft = Integer.toString((int)totalTime%60);
        if(secondleft.length()<2){
            secondleft = "0"+secondleft;
        }
        timelefttext.setText(minleft+":"+secondleft);
        timeelapsedtext.setText("0:00");
    }

    void updateTime(){
        double totalTime = mediaPlayer.getDuration()/1000.0;
        double timeElapsed = mediaPlayer.getCurrentPosition()/1000.0;
        String mine = Integer.toString((int)timeElapsed/60);
        String seconde = Integer.toString((int)timeElapsed%60);
        if(seconde.length()<2){
            seconde = "0"+seconde;
        }
        timeelapsedtext.setText(mine+":"+seconde);

        double timeLeft = totalTime - timeElapsed;
        String minleft = Integer.toString((int)timeLeft/60);
        String secondleft = Integer.toString((int)timeLeft%60);
        if(secondleft.length()<2){
            secondleft = "0"+secondleft;
        }
        timelefttext.setText(minleft+":"+secondleft);

    }
}