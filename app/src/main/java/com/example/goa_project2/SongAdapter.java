package com.example.goa_project2;

import android.content.Context;
import android.media.MediaMetadataRetriever;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.content.Intent;
import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;

public class SongAdapter extends RecyclerView.Adapter<SongViewHolder> {
    //constructor
    SongAdapter(@NonNull Context context, @NonNull ArrayList<Song> songs, @NonNull PlaylistActivity playlistActivity){
    this.context = context;
    this.songs = songs;
    this.playlistActivity = playlistActivity;
    }

    @NonNull
    @Override
    public SongViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        //method called whenever I need to create a new viewholder
        LayoutInflater inflater = LayoutInflater.from(context);
        View itemView = inflater.inflate(R.layout.playlist_item, parent, false);
        SongViewHolder viewHolder = new SongViewHolder(itemView);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull SongViewHolder holder, int position) {
    //called whenever an existing ViewHolder needs to be re-used
        //at this point we need to repopulate the viewholder
        Song song = songs.get(position);
        MediaMetadataRetriever mmr = new MediaMetadataRetriever();
        holder.imageView.setImageResource(song.artworkImage);
        holder.artistNameTextView.setText(song.artist);
        holder.songNameTextView.setText(song.name);
        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
               //TODO: implement Switch to player screen
                Intent intent = new Intent(context, PlayerScreen.class);
                intent.putExtra("index", position);
                intent.putExtra("playlist",playlistActivity.playlist1);
                context.startActivity(intent);
            }
        });
    }

    @Override
    public int getItemCount() {
        //called whenever the RecyclerView needs to know how many items to display
        return songs.size();
    }

    //properties
    Context context;
    ArrayList<Song> songs;
    PlaylistActivity playlistActivity;



}
