package com.example.goa_project2;

import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import org.w3c.dom.Text;

public class SongViewHolder extends RecyclerView.ViewHolder {


    public SongViewHolder(@NonNull View itemView) {
        super(itemView);

        this.itemView = itemView;
        imageView = itemView.findViewById(R.id.image);
        songNameTextView =  itemView.findViewById(R.id.songName);
        artistNameTextView = itemView.findViewById(R.id.artistName);
        timeTextView = itemView.findViewById(R.id.time);
    }

    //properties
    View itemView;
    ImageView imageView;
    TextView songNameTextView;
    TextView artistNameTextView;
    TextView timeTextView;
}
